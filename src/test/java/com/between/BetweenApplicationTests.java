package com.between;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.between.controller.PriceController;
import com.between.model.Prices;

@SpringBootTest
class BetweenApplicationTests {
	
	@Autowired
	private PriceController priceController;

	@Test
	void findPricePriority1() {
		Prices p = priceController.findPrice(LocalDateTime.parse("2020-06-14T10:00:00"), 35455L, 1L);
		System.err.println("findPricePriority1 " +p.getPrice());
		assertEquals(p.getPrice(), 35.5, 0.001);
	}
	
	@Test
	void findPricePriority2() {
		Prices p = priceController.findPrice(LocalDateTime.parse("2020-06-14T16:00:00"), 35455L, 1L);
		System.err.println("findPricePriority2 " +p.getPrice());
		assertEquals(p.getPrice(), 25.45, 0.001);
	}
	
	@Test
	void findPricePriority3() {
		Prices p = priceController.findPrice(LocalDateTime.parse("2020-06-14T21:00:00"), 35455L, 1L);
		System.err.println("findPricePriority3 " +p.getPrice());
		assertEquals(p.getPrice(), 35.5, 0.001);
	}
	@Test
	void findPricePriority4() {
		Prices p = priceController.findPrice(LocalDateTime.parse("2020-06-15T10:00:00"), 35455L, 1L);
		System.err.println("findPricePriority4 " +p.getPrice());
		assertEquals(p.getPrice(), 30.5, 0.001);
	}

	@Test
	void findPricePriority5() {
		Prices p = priceController.findPrice(LocalDateTime.parse("2020-06-16T21:00:00"), 35455L, 1L);
		System.err.println("findPricePriority5 " +p.getPrice());
		assertEquals(p.getPrice(), 38.95, 0.001);
	}

}
