package com.between.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.between.model.Prices;

@Repository
public interface PriceRepository extends JpaRepository<Prices, Long> {
	
	@Query("select p from Prices p where ?1 between p.startDate and p.endDate and p.productId=?2 and p.brandId=?3")
	public List<Prices> findByDateProductAndBrand(LocalDateTime date, Long productId, Long brandId);
	
}
