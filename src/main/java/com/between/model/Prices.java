package com.between.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table
public class Prices {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name= "PRICE_LIST")
	private Long priceList;
	@Column(name= "BRAND_ID")
	private Long brandId;
	@Column(name= "START_DATE")
	private LocalDateTime startDate;
	@Column(name= "END_DATE")
	private LocalDateTime endDate;
	@Column(name= "PRODUCT_ID")
	private Long productId;
	@Column(name= "PRIORITY")
	private Integer priority;
	@Column(name= "PRICE")
	private Double price;
	@Column(name= "CURR")
	private String currency;
	
}
