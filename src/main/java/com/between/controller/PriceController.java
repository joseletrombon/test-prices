package com.between.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.between.model.Prices;
import com.between.service.PriceService;
import com.between.utils.Constants;


@RestController
@RequestMapping(Constants.URL_BASE_API + Constants.API_VERSION +Constants.URL_PRICES)
public class PriceController{
	
	@Autowired
	private PriceService service;
	
	@GetMapping
	@ResponseBody
	@ResponseStatus(code = HttpStatus.OK)
	public Prices findPrice(
			@RequestParam(required = true, name = "date") 
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)LocalDateTime date,
			@RequestParam(required = true, name = "productId") Long productId,
			@RequestParam(required = true, name = "brandId") Long brandId) {
		return service.findPrice(date, productId, brandId);
	}

}
