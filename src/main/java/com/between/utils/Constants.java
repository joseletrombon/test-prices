package com.between.utils;

public class Constants {
	public static final String URL_BASE_API = "/between/api/";
	public static final String URL_PRICES = "/prices";
	public static final String API_VERSION = "v1";
}
