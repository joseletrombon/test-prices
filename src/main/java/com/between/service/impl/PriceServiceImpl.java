package com.between.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.between.model.Prices;
import com.between.repository.PriceRepository;
import com.between.service.PriceService;

@Service
public class PriceServiceImpl implements PriceService {

	@Autowired
	private PriceRepository repo;

	@Override
	public Prices findPrice(LocalDateTime date, Long productId, Long brandId) {
		List<Prices> prices = repo.findByDateProductAndBrand(date, productId, brandId);
		if (prices.size() > 1) {
			for (Prices p : prices) {
				if (p.getPriority() == 1)
					return p;
			}
		}
		return prices.get(0);

	}

	@Override
	public List<Prices> findall() {
		return repo.findAll();
	}

}
