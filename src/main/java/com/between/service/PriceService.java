package com.between.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import com.between.model.Prices;

@Service
public interface PriceService{

	Prices findPrice(LocalDateTime date, Long productId, Long brandId);

	List<Prices> findall();

}
